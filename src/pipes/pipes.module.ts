import { NgModule } from '@angular/core';

import { HashPipe } from './hash/hash';
import { DifficultyToHashPipe } from './difficulty-to-hash/difficulty-to-hash';
import { XmrPipe } from './xmr/xmr';
import { HashToLinkPipe } from './hash-to-link/hash-to-link';
import { ConvertCurrencyPipe } from './convert-currency/convert-currency';
import { FromNowPipe } from './from-now/from-now';
import { WherePipe } from './where/where';
import { PrettySettingsPipe } from './pretty-settings/pretty-settings';
import { MainLoopPipe } from './main-loop/main-loop';
import { PoolTypePipe } from './pool-type/pool-type';
import { WhereTruePipe } from './where-true/where-true';

@NgModule({
    declarations: [
        HashPipe,
        DifficultyToHashPipe,
        XmrPipe,
        HashToLinkPipe,
        ConvertCurrencyPipe,
        FromNowPipe,
        WherePipe,
    PrettySettingsPipe,
    MainLoopPipe,
    PoolTypePipe,
    WhereTruePipe
    ],
    imports: [],
    exports: [
        HashPipe,
        DifficultyToHashPipe,
        XmrPipe,
        HashToLinkPipe,
        ConvertCurrencyPipe,
        FromNowPipe,
        WherePipe,
    PrettySettingsPipe,
    MainLoopPipe,
    PoolTypePipe,
    WhereTruePipe
    ]
})
export class PipesModule { }
